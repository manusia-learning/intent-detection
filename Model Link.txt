The model is too large to fit in the Scele submission slot. It is stored in the following link.
JointBERT-SlotRefine: https://drive.google.com/drive/folders/1vuf4IZYyoInyYdise5S9LZE9003Lb6sN?usp=sharing
LSTM: https://drive.google.com/drive/folders/105ETAA9leXX3tQfJ1pWKGJgV_uf0R91j?usp=sharing
GoogleNews-vectors-negative300.bin.gz: https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?resourcekey=0-wjGZdNAUop6WykTtMip30g